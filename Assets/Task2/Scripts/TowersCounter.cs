﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowersCounter : MonoBehaviour {
    public int count;
    public Text countText;
    public bool limitWasReached = false;
    public List<GameObject> towers = new List<GameObject>();
    public void AddTower(GameObject tower) {
 
        towers.Add(tower);

        if(towers.Count >= 100) {
            limitWasReached = true;
            foreach(GameObject twr in towers) {
                twr.GetComponent<TowerController>().Start();
            }
        }

        countText.text = "Towers: " + towers.Count.ToString();
    }
}
