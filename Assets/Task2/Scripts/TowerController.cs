﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour {
    public GameObject bulletPrefab;
    public float delay = 0f;
    int shootsCounter = 0;
	// Use this for initialization
	private void Awake()
	{
        
        GameObject.Find("Scene").GetComponent<TowersCounter>().AddTower(this.gameObject);
	}

	public void Start () {
        gameObject.tag = "Old";
        InvokeRepeating("Rotate", delay, 0.5f);
	}

    void Rotate() {
       
        if (shootsCounter < 12)
        {
            this.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
            transform.Rotate(Vector3.up, Random.Range(15, 45));
            Shoot();
        } else {
            CancelInvoke();
        }
    }

    void Shoot() {
        shootsCounter++;
        Vector3 offset = new Vector3();
        offset = transform.position + transform.forward * 0.6f;
        Debug.Log(transform.position);
        Debug.Log(offset);
 
        GameObject bullet = Instantiate(bulletPrefab, offset, transform.rotation);
    }

}
