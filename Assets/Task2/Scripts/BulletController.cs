﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    float distanceToTravel;
    Vector3 startPos;
    public GameObject tower;
     
	// Use this for initialization
	void Awake () {
        this.distanceToTravel = Random.Range(1.0f, 4.0f);
        startPos = transform.position;
  	}
	
    //Using FixedUpdate to have fixed speed, 200f used as multiplayed for fixedDeltaTime to archive 4 units/per second speed
	void FixedUpdate () {
      
        if (Vector3.Distance(startPos, transform.position) < distanceToTravel)
        {
            transform.localPosition +=  transform.forward * 4f * Time.fixedDeltaTime;
        } else {
            if (!GameObject.Find("Scene").GetComponent<TowersCounter>().limitWasReached)
            {
                GameObject newTower = Instantiate(tower, transform.position, Quaternion.identity) as GameObject;
                newTower.GetComponent<TowerController>().delay = 6f;
                newTower.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
            }
            Destroy(this.gameObject);
        }
	}

	private void OnTriggerEnter(Collider other)
	{
        if (other.gameObject.tag == "Old")
        {
            GameObject.Find("Scene").GetComponent<TowersCounter>().towers.Remove(other.gameObject);
            Destroy(other.gameObject);
          
        }
	}
}
